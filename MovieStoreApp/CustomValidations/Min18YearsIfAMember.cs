﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using MovieStoreApp.Entities;

namespace MovieStoreApp.CustomValidations
{
    public class Min18YearsIfAMember: ValidationAttribute
    {
        //overide method has two overload go for the second one because it has object 
        // so that you can reach the other properties of the object

        //apply this to our class
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
           //select the membershipType

            //gives access to the containing class
            //validationContext.ObjectInstance
            //becuase this is object we need to cast this to the customer

            var customer = (Customers) validationContext.ObjectInstance;
            //pay as you go
            //Or when the user doesnt select a membership type
            if (customer.MembershipTypeId == MembershipTypes.Unkown || customer.MembershipTypeId== MembershipTypes.PayAsYouGo)
                //staticfiledsuccess
                return ValidationResult.Success;

            //for the error state
            if (customer.Birthdate==null)
                return  new ValidationResult("Birthdate is required");

            //we have the birthdate we can calculate the age
            var age = DateTime.Today.Year - customer.Birthdate.Value.Year;

            return (age >= 18)
                ? ValidationResult.Success
                : new ValidationResult("Customer should be at least 18 years old");
            //add validation message to the birth date
        }
    }
}