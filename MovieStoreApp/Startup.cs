﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MovieStoreApp.Startup))]
namespace MovieStoreApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
