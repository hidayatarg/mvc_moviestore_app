﻿using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using MovieStoreApp.Models;
using MovieStoreApp.Entities;

namespace MovieStoreApp.Models
{
      

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Customers> Customers { get; set; }
        public DbSet<Movies> Movies { get; set; }
        public DbSet<MembershipTypes> MembershipTypes { get; set; }
        public DbSet<Genre> Genres { get; set; }
       
        public ApplicationDbContext() : base("DefaultConnection", throwIfV1Schema: false)
        {

        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}