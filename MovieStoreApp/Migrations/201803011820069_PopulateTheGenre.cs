namespace MovieStoreApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateTheGenre : DbMigration
    {
        public override void Up()
        {

            Sql("INSERT INTO Genre (Id, Name) VALUES (1, 'Action')");
            Sql("INSERT INTO Genre (Id, Name) VALUES (2, 'Thriller')");
            Sql("INSERT INTO Genre (Id, Name) VALUES (3, 'Family')");
            Sql("INSERT INTO Genre (Id, Name) VALUES (4, '+18')");
            Sql("INSERT INTO Genre (Id, Name) VALUES (5, 'Comdey')");
            Sql("INSERT INTO Genre (Id, Name) VALUES (6, 'Documentry')");
        }
        
        public override void Down()
        {
        }
    }
}
