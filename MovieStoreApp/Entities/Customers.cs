﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using MovieStoreApp.CustomValidations;

namespace MovieStoreApp.Entities
{
    [Table("Customers")]
    public class Customers
    {
        public int Id { get; set; }
        
        //custom validation normally the automatic messages work fine
        [Required(ErrorMessage = "Please enter the customer name!")]
        [StringLength(255)]
        public string Name { get; set; }

        public bool IsSubscribedToNewsletter { get; set; }

        public MembershipTypes MembershipType { get; set; }
        [Display(Name = "Membership Type")]
        public byte MembershipTypeId { get; set; }

       
        [Display(Name = "Date of Birth")]
        [Min18YearsIfAMember]
        public DateTime? Birthdate { get; set; }

       
    }
}