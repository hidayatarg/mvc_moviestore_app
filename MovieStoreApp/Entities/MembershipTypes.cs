﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MovieStoreApp.Entities
{
    [Table("MembershipTypes")]
    public class MembershipTypes
    {
        public byte Id { get; set; }
        [Required]
        public string Name { get; set; }
        public short SignUpFee { get; set; }
        public byte DurationInMonths { get; set; }
        public byte DiscountRate { get; set; }

        //for getting rid of the magic strings

        //also refactoring a magic string to use an enum
        //if we use an enum we need to cast it to byte to compare with
       // customer.MembershipTypeId ==(byte) MembershipTypes.Unkown other approch
        public static readonly byte Unkown = 0;
        public static readonly byte PayAsYouGo = 1;

    }
}