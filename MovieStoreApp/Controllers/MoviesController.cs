﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MovieStoreApp.Entities;
using MovieStoreApp.Models;
using MovieStoreApp.ViewModels;

namespace MovieStoreApp.Controllers
{
    public class MoviesController : Controller
    {
        private ApplicationDbContext _context;

        public MoviesController()
        {
            _context=new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: Movies
        public ActionResult Index()
        {
            var movie = _context.Movies.Include(m => m.Genre).ToList();
            return View(movie);
        }

        // GET: Movies/5
        public ActionResult Detail(int id)
        {
            var movieDetails = _context.Movies.Include(m => m.Genre).SingleOrDefault(c => c.Id == id);
            if (movieDetails == null)
                return HttpNotFound();

            return View(movieDetails);

         
        }

        public ActionResult New()
        {
            var genre = _context.Genres.ToList();
            var viewModel = new NewMoviesViewModel()
            {
                Genres = genre
            };

            return View("MovieForm",viewModel);
        }

        public ActionResult Save(Movies movies)
        {
            

            if (movies.Id == 0)
                _context.Movies.Add(movies);
            else
            {
                var movieInDb = _context.Movies.Single(c => c.Id == movies.Id);

                movieInDb.Name = movies.Name;
                movieInDb.DateAdded = movies.DateAdded;
                movieInDb.ReleaseDate = movies.ReleaseDate;
                movieInDb.GenreId = movies.GenreId;
               
                movieInDb.NumberInStock = movies.NumberInStock;
               
            
            }

            _context.SaveChanges();
            return RedirectToAction("Index", "Movies");
        }

        public ActionResult Edit(int id)
        {
            var movie = _context.Movies.SingleOrDefault(c => c.Id == id);
          
            if (movie == null)
            return HttpNotFound();
            
                var viewModel = new NewMoviesViewModel()
                {
                    Movies = movie,
                    Genres = _context.Genres.ToList()
                };
            
            return View("MovieForm", viewModel);
        }


        public ActionResult Delete(int id)
        {
            var movie = _context.Movies.SingleOrDefault(c => c.Id == id);

            if (movie == null)
                return HttpNotFound();
            
            //remove in memory just
            _context.Movies.Remove(movie);
            
            //save in the db
            _context.SaveChanges(); 
            
            return RedirectToAction("Index", "Movies");
        }



    }
}