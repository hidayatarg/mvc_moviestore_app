﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MovieStoreApp.Models;
using System.Data.Entity;
using System.Data.Entity.Validation;
using MovieStoreApp.Entities;
using MovieStoreApp.ViewModels;

namespace MovieStoreApp.Controllers
{
    public class CustomerController : Controller
    {
        private ApplicationDbContext _context;

        public CustomerController()
        {
            _context= new ApplicationDbContext();  
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: Customer
        public ActionResult Index()
        {
            var customer= _context.Customers
                .Include(m => m.MembershipType)
                .ToList();
            return View(customer);
        }


        public ActionResult Detail(int id)
        {
            var customerDetails = _context.Customers.Include(m => m.MembershipType).SingleOrDefault(c => c.Id == id);
            if (customerDetails == null)
                return HttpNotFound();

            return View(customerDetails);


        }

        public ActionResult New()
        {
           //we get an error message with error summary for id so we intialize the customer
            var viewModel = new NewCustomerViewModel
            {
                Customers = new Customers(),
                MembershipTypes = _context.MembershipTypes.ToList()
            };

            return View("CustomerForm", viewModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Customers customers)
        {
            //1-step add data annotations
            //2-add the following block
            //3-step adding validation messages to the form
            //4-step adding css class in the site
            //if model state is not valid we return the same view
            if (!ModelState.IsValid)
            {
                var viewModel = new NewCustomerViewModel
                {
                    Customers = customers,
                    MembershipTypes = _context.MembershipTypes.ToList()
                };
                return View("CustomerForm", viewModel);
            }
            

            if (customers.Id == 0)
                _context.Customers.Add(customers);
            else
            {
                var customerInDb = _context.Customers.Single(c => c.Id == customers.Id);

                customerInDb.Name = customers.Name;
                customerInDb.MembershipTypeId = customers.MembershipTypeId;
                customerInDb.Birthdate = customers.Birthdate;
                customerInDb.MembershipTypeId = customers.MembershipTypeId;
                customerInDb.IsSubscribedToNewsletter = customers.IsSubscribedToNewsletter;
                
            }


            _context.SaveChanges();
            return RedirectToAction("Index", "Customer");
        }




        public ActionResult Edit(int id)
        {
            var customer = _context.Customers.SingleOrDefault(c => c.Id == id);

            if (customer == null)
                return HttpNotFound();

            var viewModel = new NewCustomerViewModel
            {
                Customers = customer,
                MembershipTypes = _context.MembershipTypes.ToList()
            };

            return View("CustomerForm", viewModel);
        }


        public ActionResult Delete(int id)
        {
            var customer = _context.Customers.SingleOrDefault(c => c.Id == id);

            if (customer == null)
                return HttpNotFound();
            
            _context.Customers.Remove(customer);
            _context.SaveChanges();
            return RedirectToAction("Index", "Customer");
        }

    }
}