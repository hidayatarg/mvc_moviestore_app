﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MovieStoreApp.Entities;

namespace MovieStoreApp.ViewModels
{
    public class NewMoviesViewModel
    {
        public IEnumerable<Genre> Genres { get; set; }
        public Movies Movies { get; set; }
    }
}